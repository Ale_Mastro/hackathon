import React from 'react'

export default function Message({user, text}) {
  return (
    <div className={`mt-5 py-3 px-10 text-right w-[70%]  flex rounded-b-2xl  border  ${user ? "mr-3 float-right bg-[#fdfcfd] rounded-tl-2xl" : "bg-[#c6e1ff] ml-3 rounded-tr-2xl" }`}>
        <p className='w-full break-words'>

        {text}
        </p>
        </div>
  )
}
