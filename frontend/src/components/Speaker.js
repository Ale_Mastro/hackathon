import React, { useEffect, useState } from 'react';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';
import Message from './Message';
import { FaMicrophone, FaMicrophoneSlash } from "react-icons/fa";
import ScaleLoader from "react-spinners/ScaleLoader";
import { continueChat, getChat, startChat } from '../api';

const Dictaphone = () => {
  const {
    transcript,
    listening,
    resetTranscript,
    browserSupportsSpeechRecognition
  } = useSpeechRecognition();
  

  const [conversation, setConversation] = useState([]);
  const [playsound, setPlaysound] = useState(null);
  const [text, setText] = useState("");
  const [firstTime, setFirstTime] = useState(false);
  const [voice, setVoice] = useState(false);


  const [ctx, setCtx] = useState(null);
  if (!browserSupportsSpeechRecognition) {
    return <span>Browser doesn't support speech recognition.</span>;
  }
  const startListening = () => {
    if (ctx){
    playsound.stop(ctx.currentTime);}
    SpeechRecognition.startListening({ continuous: true });
  }
  const stopListening = async () => {
    SpeechRecognition.stopListening()
    const tmp1 = [...conversation, { user: true, text: voice ?  transcript : text }];
    if (!firstTime){
    const stringFromOpenAI = await startChat(voice ?  transcript : text);
    const tmp1 = [...conversation, { user: true, text: voice ?  transcript : text }, { user: false, text: stringFromOpenAI }]
    setConversation(tmp1)
    setText("")
      setFirstTime(true)
  } else {
    const stringFromOpenAI = await continueChat(voice ?  transcript : text);
    const tmp1 = [...conversation, { user: true, text: voice ?  transcript : text }, { user: false, text: stringFromOpenAI }]
    setConversation(tmp1)
    setText("")
  }
  resetTranscript()

  }
  return (
    <div className='absolute right-0 left-0 ml-[0] pb-20 bg-gradient-to-t from-[#98b9e1] to-white h-full '>
      <div className='relative w-full h-full overflow-auto'>
        {conversation.map((msg, index) => (<Message {...msg} key={index} />))}
      </div>
        <div className='absolute flex overflow-hidden text-ellipsis w-full bottom-[20px] px-10'>
          <div className='flex w-full pr-2'>
            <input type='text' className=" border-[#7b7979] border-[2px] rounded-md w-full px-4 py-2" value={text} onChange={(v)=> setText(v.target.value)}/>
            {!voice && <button onClick={stopListening} className='hover:bg-gray-400
        bg-[#2066f1] text-white p-3 rounded-full ml-3 justify-center items-center'>Send</button>}


          {voice && <>  {!listening ?
              <button onClick={startListening} className='
              hover:bg-gray-400
              bg-[#2066f1] text-white p-4 rounded-full ml-3 justify-center items-center'>
                <FaMicrophone />
              </button> :
              <div className='flex gap-2 mx-3'>
                <ScaleLoader
                  color={"#ffffff"}
                  loading={true}
                  size={10}
                />
                <button onClick={stopListening} className='text-red-600'>
                  <FaMicrophoneSlash />
                </button>
              </div>
            }</>}
          <label class="mx-4 relative flex justify-center items-center cursor-pointer">
            <input type="checkbox" onChange={(e) => setVoice(e.target.checked)} class="sr-only peer" defaultValue={false}/>
            <div class="w-11 h-6 bg-gray-200
            peer-focus:outline-none peer-focus:ring-4
            peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800
            rounded-full peer dark:bg-gray-700
            peer-checked:after:translate-x-full peer-checked:after:border-white
            after:content-['']
            after:absolute
            after:top-[14px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
            <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">Voice</span>
          </label>
          </div>

        </div>
    </div>
  );
};
export default Dictaphone;