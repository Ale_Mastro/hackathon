import axios from "axios"

export async function startChat(msg) {
    const {data} = await axios.post(`http://localhost:5000/start-chat/?user_text=${msg}/`);
    
    
    const url = "https://api.elevenlabs.io/v1/text-to-speech/21m00Tcm4TlvDq8ikWAM";
    const headers = {
      "Accept": "audio/mpeg",
      "Content-Type": "application/json",
      "xi-api-key": "" //TODO: insert you apikey
    };
    const datad = {
      "text": data,
      "model_id": "eleven_monolingual_v1",
      "voice_settings": {
        "stability": 0.5,
        "similarity_boost": 0.5
      }
    };
    return axios.post(url, datad, { headers, responseType: 'blob' })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const audio = document.createElement('audio');
        audio.classList = 'hidden'
        audio.src = url;
        audio.controls = true;
        audio.autoplay = true;
        document.body.appendChild(audio);
        return data
      })
      .catch(error => {
        console.error('Error downloading audio file:', error);
      });

}


export async function continueChat(msg) {
    const {data} = await axios.post(`http://localhost:5000/chat/?user_input=${msg}/`);
    
    const url = "https://api.elevenlabs.io/v1/text-to-speech/21m00Tcm4TlvDq8ikWAM";
    const headers = {
      "Accept": "audio/mpeg",
      "Content-Type": "application/json",
      "xi-api-key": "" //TODO: insert you apikey
    };
    const datad = {
      "text": data,
      "model_id": "eleven_monolingual_v1",
      "voice_settings": {
        "stability": 0.5,
        "similarity_boost": 0.5
      }
    };
    return axios.post(url, datad, { headers, responseType: 'blob' })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const audio = document.createElement('audio');
        audio.src = url;
        audio.classList = 'hidden'

        audio.controls = true;
        audio.autoplay = true;
        document.body.appendChild(audio);
        return data
      })
      .catch(error => {
        console.error('Error downloading audio file:', error);
      });

}