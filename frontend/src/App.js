import logo from './logo.svg';
import './App.css';
import Dictaphone from './components/Speaker';
import AudioRecorder from './components/AudioRecorder';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Sidebar from './components/Sidebar';

function App() {
  return (
    <div className="App">
       <Navbar/>
      <div className="w-full absolute top-[64px] bottom-[50px]">
        {/* <Sidebar /> */}
        <Dictaphone />

      </div>
      <Footer/>
    </div>
  );
}

export default App;
