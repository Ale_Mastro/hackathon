
# Laura AI Tutor

## Description

Laura AI Tutor is an intelligent tutoring system designed to assist users in their learning path by adapting and improving alongside the user. It utilizes the power of Language Models (LLM) to provide personalized and interactive learning experiences.

This project was born out of an inspiring Hackathon event, where a team of dedicated developers came together to create an innovative solution for AI-powered tutoring. With a focus on user-centric learning, Laura AI Tutor aims to enhance the educational journey by providing tailored guidance and support.

The project is built using React for the frontend and Python for the backend. The frontend interface allows users to interact with the AI tutor, while the backend handles the processing of user input and communication with external APIs. Laura AI Tutor leverages the OpenAI API for natural language processing and generation, as well as the Eleven Labs API for text-to-speech functionality.

## Installation

To install and run the Laura AI Tutor project locally, follow the steps below:

1. Clone the repository:

```bash
git clone https://gitlab.com/Ale_Mastro/hackathon.git
```

2. Navigate to the project directory:


3. Install the dependencies for the frontend:

```bash
npm install
```

4. Install the dependencies for the backend:


```bash

pip install -r requirements.txt


```

5. Set up API keys:

- In the backend directory, open the `config.py` file and insert your OpenAI API key in the designated TODO section.
- In the frontend directory, open the `.env` file and insert your free API key for the Eleven Labs API in the designated TODO section.

6. Start the backend server:

```bash
python app.py
```

7. In a separate terminal window, start the frontend development server:


```bash
npm start
```


8. Open your web browser and navigate to `http://localhost:3000` to access Laura AI Tutor.

## Usage

Once the Laura AI Tutor application is running, follow these steps to interact with the AI tutor:

1. Enter your learning goals and preferences in the input field provided.

2. Laura AI Tutor will process your input using the OpenAI API and generate a response tailored to your needs.

3. The response will be displayed on the screen, and if configured correctly, it will also be converted into speech using the Eleven Labs API.

4. Engage in a conversation with Laura AI Tutor by providing further input or asking questions.

5. Laura AI Tutor will continuously learn from your interactions and adapt its responses to provide a better learning experience over time.

## Infrastructure

Inside the project, you will find a directory named `infrastructure`, which contains the necessary files to launch the application using Docker Compose. If you prefer to deploy the application using containers, follow these steps:

1. Navigate to the `infrastructure` directory:


```bash
cd infrastructure
```

2. Launch the Docker Compose configuration:


```bash
docker-compose up -d
```

3. The frontend and backend services will be deployed within their respective containers.

## Contributing

If you would like to contribute to the development of Laura AI Tutor, please follow these guidelines:

1. Fork the repository on GitHub.

2. Create a new branch with a descriptive name for your feature or bug fix.

3. Make your modifications and ensure the codebase remains clean and well-documented.

4. Test your changes thoroughly.

5. Commit and push your changes to your forked repository.

6. Submit a pull request to the main repository, clearly describing the changes you have made.

7. Your pull request will be reviewed by the project maintainers, and any necessary feedback or changes will be discussed.

8. Once approved, your changes will be merged into the main project.

## License

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for more information.

## Acknowledgements

We would like to express our gratitude to the following individuals and organizations for their contributions to the Laura AI Tutor project:

- OpenAI for providing the powerful language models used in this project.
- Eleven Labs for their text-to-speech API, enabling audio output for a richer user experience.

## Contact

If you have any questions, suggestions, or feedback regarding the Laura AI Tutor project, please feel free to contact us at [email protected]
