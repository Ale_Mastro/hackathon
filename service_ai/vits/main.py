from fastapi import FastAPI
from fastapi.responses import FileResponse, PlainTextResponse, StreamingResponse
from fastapi.middleware.cors import CORSMiddleware

import os.path as osp

from tts import generate_audio
from openai_mod import start_eng_bot, interaction

app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:3000",
] 
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
last_level = "Beginner"


@app.post("/chat", response_class=PlainTextResponse)
def chat(user_input: str, user_level: str="Beginner"):
  global last_level
  level, response = interaction(user_input, last_level)
  last_level = level
  return response
  

@app.post("/start-chat", response_class=PlainTextResponse)
def start_char(user_text: str):
  global last_level
  level, response = start_eng_bot(user_text)
  print("---", response)
  last_level = level
  return response


@app.post("/text_to_speech/")
def text_to_speech(text: str):
  file_path = generate_audio(text)
  # file_path = osp.basename(text)
  # def iterfile():
  #   with open(file_path, "rb") as f:
  #     yield from f
  print("ììììììì", file_path)
  assert osp.isfile(file_path)
  headers = {'Content-Disposition': f'attachment; filename="{file_path}"'}
  return FileResponse(file_path, headers=headers, media_type="audio/mp3")

if __name__ == "__main__":
  import uvicorn
  uvicorn.run("main:app", host="0.0.0.0", port=5000, reload=True)
