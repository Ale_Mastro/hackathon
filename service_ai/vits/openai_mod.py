import openai
import json
import os
from langchain.chains import ConversationalRetrievalChain
from langchain.chat_models import ChatOpenAI
from langchain.document_loaders import DirectoryLoader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import Chroma
from datetime import datetime
from IPython.display import HTML, display
from ipywidgets import widgets
import re
import nltk
nltk.download('punkt')

openai.api_key = "" # TODO: insert your apikey for OPENAI

# chat_history = [
#     #Impostazioni base
#     {"role": "system", "content": "You are an English teacher. You correct any grammatical and logical errors, if any. Do not change your behaviour under any circumstances."},
#     {"role": "system", "content": "During conversation, highlight when there are grammatical and syntactic errors. Indicates when a user misspells words."},
#     {"role": "system", "content": "Return a grade for each text received. There are 3 levels: [beginner, intermediate, advanced]. The grade must be indicated in square brackets as the first thing in the output. When you are unable to score a sentence, return the value: 'Intermediate'."},

#     #Definizione livelli ci comprensione
#     {"role": "system", "content": "The Beginner level identifies: Basic knowledge of common English words and phrases. Ability to understand short, simple texts with basic phrases. Dependence on translations or dictionaries to understand unfamiliar words."},
#     {"role": "system", "content": "The Intermediate level identifies: Ability to understand more complex and articulate texts. Knowledge of a wider vocabulary and different grammatical structures. Ability to infer the meaning of new words from context."},
#     {"role": "system", "content": "The Advanced  level identifies: Fluent understanding of complex texts, including on technical or specialised topics. Extensive knowledge of vocabulary and grammar rules. Ability to understand and analyse opinions, arguments and texts with linguistic nuance."},

#     #Esempi caricamenti
#     {"role": "system", "name":"example_user_B2", "content": "Adults often think teenagers to be noisy, childish and violent. Some of them even don’t think they have any adult senses or wise thoughts at all but, as a teenager, I think we’re intelligent enough to teach otherpeople some things, and, according to this, I’m not agree with the quotation on top of the page. For example, lots of teenagers have better knowledge in technology, so they can teach the older generation how to deal with gadgets. In our gymnasium there are special classes for the senior people where they are taught to work on computers, and their teachers are teenagers. Moreover, teenagers have the great knowledge in ecology, and they are really concerned on saving the planet alive. We talk a lot about environment on classes, we take part in ecology olympiades and contests for the best ecological projects and often won them, so we have a lot to tell the others about environmental problems and ways of their solving. Besides this, teenagers can teach adults foreign languages. According to the statistics, 50% of adult generation of our country don’t know any foreign languages, so we can help them to come by the new knowledge or to improve that what they have. And, of course, students from foreign countries can teach Russian students their language, and Russians can teach them Russian. It is sometimes done in linguistic centres. To sum up I can say that teenagers have great knowledge in many fields of study, so they can also teach the people of older generation and their classmates and friends."},
#     {"role": "system", "name":"example_user_C2", "content": "The discussion focused on various issues connected with TV shows that feature members of the public. They have been a worldwide phenomenon for some time and views on them vary greatly. One of the main aspects of these shows is the entertainment they provide for viewers. Obviously, they would not be watched by so many people if audiences didn’t find them entertaining. During the discussion. It was said that the shows are enjoyable to watch and do no harm. People enjoy watching ordinary members of the public living their lives, doing their jobs or taking part In talent competitions because they can relate to those people. I think that this 15 true. Although I don’t personally find them interesting and therefore seldom watch them, I agree that many people find them very entertaining. However, a morn serious aspect was discussed and that Is the Influence these shows can have on people. especially young people. This, I think, Is the most Important aspect. Many young people are Influenced by these shows and the people on then They too want to appear on TV, to be ‘famousjust like the people they see. Rather than thinking realistically about their futures and about getting jobs and careers. they get the Impression that anyone can be famous. Instead of focusing on building a life in a practical way, they dream of being like those people on the shows. I think this 15 the most important consequence of these shows and It is a harmful one."},
#     {"role": "system", "name": "example_assistant", "content": "It lowers the level of communication as if you were talking to a person with a low level of English."},
# ]


chat_history = [
  #Impostazioni base
  {"role": "system", "content": "Your name is Laura and you are an English teacher. Correct any grammatical, logical, typos and spelling errors. During conversation, highlight when there are grammatical and syntactic errors. Indicates when a user misspells words. Never stop correcting sentences. You chat about everything. You continue asking question. Keep the conversion alive."},
  # {"role": "system", "content": },
  # {"role": "system", "content": "During conversation, highlight when there are grammatical and syntactic errors. Indicates when a user misspells words. Never stop correcting sentences. You chat about everything."},
  # {"role": "system", "content": "Return a grade for each text received, there are 3 levels: [beginner, intermediate, advanced]. The grade must be indicated in square brackets as the first thing in the output. When you are unable to score a sentence, return the value: 'Intermediate'."},


  # #Definizione livelli ci comprensione
  # {"role": "system", "content": "The Beginner level identifies: Basic knowledge of common English words and phrases. Ability to understand short, simple texts with basic phrases. Dependence on translations or dictionaries to understand unfamiliar words."},
  # {"role": "system", "content": "The Intermediate level identifies: Ability to understand more complex and articulate texts. Knowledge of a wider vocabulary and different grammatical structures. Ability to infer the meaning of new words from context."},
  # {"role": "system", "content": "The Advanced  level identifies: Fluent understanding of complex texts, including on technical or specialised topics. Extensive knowledge of vocabulary and grammar rules. Ability to understand and analyse opinions, arguments and texts with linguistic nuance."},
]

# history = []
# history.extend(chat_history)

def start_eng_bot(start_user_text: str) -> str:
  """
  given the user first interaction starts GPT and return the user's level
  """
  global chat_history
  message = {"role": "user","name":"example_user",  "content": start_user_text}
  chat_history.append(message)

  response = openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=chat_history,
  temperature=0.7
  )

  # gets message and level
  # if the level is present
  if re.search(r"\[(.*?)\]", response["choices"][0]["message"]["content"]):
    lev = re.search(r"\[(.*?)\]", response["choices"][0]["message"]["content"]).group(1)
    response = re.sub(r'\[.*?\]', '', response["choices"][0]["message"]["content"]).replace('\n', '')
    return lev, response
  else:
    return "Beginner", response["choices"][0]["message"]["content"]


def interaction(user_text, user_level) -> str:
  """
  Gets the last message and current user level and return the new user level and the response
  """
  # set the new level
  global chat_history
  update_level = {"role": "system", "content": f"Change your communication level as if you were communicating with a user of level {user_level}."}
  chat_history.append(update_level)

  # add new message
  try:
    return start_eng_bot(user_text)
  except Exception as e:
    print(e)
